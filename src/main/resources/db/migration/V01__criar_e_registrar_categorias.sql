CREATE TABLE public.categorias
(
  id_categoria BIGSERIAL PRIMARY KEY,
  nome character varying(50)
)

WITH (
  OIDS=FALSE
);
ALTER TABLE public.categorias
  OWNER TO postgres;

INSERT INTO categorias (nome) VALUES ('TESTE1');
INSERT INTO categorias (nome) VALUES ('TESTE2');
INSERT INTO categorias (nome) VALUES ('TESTE3');
INSERT INTO categorias (nome) VALUES ('TESTE4');