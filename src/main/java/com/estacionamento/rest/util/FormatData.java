package com.estacionamento.rest.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class FormatData {

	public static Date formataData(String data) {

		try {

			return new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(data);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return new Date();
	}
}
