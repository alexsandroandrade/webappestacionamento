package com.estacionamento.rest.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "view_vagas")
public class ViewRelatorioVagas implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id_ticket")
	private Long id;
	
	@Column(name = "entrada")
	private Date entrada;
	
	@Column(name = "saida")
	private Date saida;
	
	@Column(name = "valor_total_hora")
	private Double valorTotalHora;
	
	@Column(name = "id_vaga")
	private Long vagaId;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "placa")
	private String placa;
	
	@Column(name = "cor")
	private String cor;
	
	@Column(name = "marca")
	private String marca;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getEntrada() {
		return entrada;
	}

	public void setEntrada(Date entrada) {
		this.entrada = entrada;
	}

	public Date getSaida() {
		return saida;
	}

	public void setSaida(Date saida) {
		this.saida = saida;
	}

	public Double getValorTotalHora() {
		return valorTotalHora;
	}

	public void setValorTotalHora(Double valorTotalHora) {
		this.valorTotalHora = valorTotalHora;
	}

	public Long getVagaId() {
		return vagaId;
	}

	public void setVagaId(Long vagaId) {
		this.vagaId = vagaId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	
	

	
	

}
