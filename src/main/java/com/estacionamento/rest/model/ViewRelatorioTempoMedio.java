package com.estacionamento.rest.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "view_tempo_medio")
public class ViewRelatorioTempoMedio implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "saida")
	private Date saida;
	@Column(name = "entrada")
	private Date entrada;
	@Column(name = "tempo_permanencia")
	private Date tempoPermanencia;
	

	public Date getSaida() {
		return saida;
	}

	public void setSaida(Date saida) {
		this.saida = saida;
	}

	public Date getEntrada() {
		return entrada;
	}

	public void setEntrada(Date entrada) {
		this.entrada = entrada;
	}

}
