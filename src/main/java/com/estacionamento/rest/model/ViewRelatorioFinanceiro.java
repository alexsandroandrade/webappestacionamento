package com.estacionamento.rest.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "view_financeiro")
public class ViewRelatorioFinanceiro implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "entrada")
	private Date entrada;
	@Column(name = "valor")
	private Double valor;
}
