package com.estacionamento.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author Alexsandro Andrade
 *
 */
@SpringBootApplication
public class WebappestacionamentoApplication {


	public static void main(String[] args) {
		SpringApplication.run(WebappestacionamentoApplication.class, args);
	}

}
