package com.estacionamento.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estacionamento.rest.model.Vaga;
import com.estacionamento.rest.service.VagaService;

@RestController
@RequestMapping("/vagas")
public class VagaResource {
	
	@Autowired
	private VagaService vagaService;
	
	@PostMapping
	public Vaga cadastrar(@RequestBody Vaga vaga) {
		return vagaService.save(vaga);
	}

}
