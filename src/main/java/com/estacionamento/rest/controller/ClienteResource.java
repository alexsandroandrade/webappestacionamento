package com.estacionamento.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estacionamento.rest.model.Cliente;
import com.estacionamento.rest.service.ClienteService;
import com.fasterxml.jackson.annotation.JsonIgnore;


@RestController
@RequestMapping("/clientes")
public class ClienteResource {

	@Autowired
	private ClienteService clienteService;
	
	@JsonIgnore
	@GetMapping("/{id}")
	public ResponseEntity<Cliente> findById(@PathVariable Long id) {
		return clienteService.findById(id).map(cliente -> ResponseEntity.ok(cliente)).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@GetMapping
	public List<Cliente> findAll() {
		return clienteService.findAll();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Cliente> update(@RequestBody Cliente clienteParam, @PathVariable Long id){
		return clienteService.findById(id).map(cliente -> {
			cliente.setNome(clienteParam.getNome());
			cliente.setTelefone(clienteParam.getTelefone());
			return ResponseEntity.ok(clienteService.save(cliente));
		}).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@PostMapping
	public Cliente cadastrar(@Valid @RequestBody Cliente cliente) {
		return clienteService.save(cliente);
	}
	
	@DeleteMapping("/{id}") 
	public ResponseEntity<Object> delete(@PathVariable Long id){
		return clienteService.findById(id).map(cliente -> {
			clienteService.delete(cliente);
			return ResponseEntity.noContent().build(); 
		}).orElseGet(() -> ResponseEntity.notFound().build());
	}

}
