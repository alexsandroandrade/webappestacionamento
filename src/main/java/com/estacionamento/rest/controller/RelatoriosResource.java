package com.estacionamento.rest.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.estacionamento.rest.model.ViewRelatorioFinanceiro;
import com.estacionamento.rest.model.ViewRelatorioTempoMedio;
import com.estacionamento.rest.model.ViewRelatorioVagas;
import com.estacionamento.rest.repository.ViewRelatorioFinanceiroRepository;
import com.estacionamento.rest.repository.ViewRelatorioTempoMedioRepository;
import com.estacionamento.rest.repository.ViewRelatorioVagasRepository;
import com.estacionamento.rest.util.FormatData;

@RestController
@RequestMapping("/relatorios")
public class RelatoriosResource {

	@Autowired
	private ViewRelatorioVagasRepository viewRelatorioVagasRepository;
	@Autowired
	private ViewRelatorioTempoMedioRepository viewRelatorioTempoMedioRepository;
	@Autowired
	private ViewRelatorioFinanceiroRepository viewRelatorioFinanceiroRepository;
	@Autowired
	FormatData formatData;
	
	@GetMapping("/tickets")	
	public List<ViewRelatorioVagas> FindAllTickets() {
		return viewRelatorioVagasRepository.findAll();
	}

	@GetMapping("/tempo-medio")
	public List<ViewRelatorioTempoMedio> FindTempoMedioByData(@RequestParam("data-entrada") String entrada,	@RequestParam("data-saida") String saida) {		

		Date inicio = FormatData.formataData(entrada + " 00:00");
		Date fim = FormatData.formataData(saida + " 23:59");;
		
		
		return viewRelatorioTempoMedioRepository.findByEntradaBetween(inicio , fim);
	}
	
	@GetMapping("/financeiro")
	public List<ViewRelatorioFinanceiro> FindValorByData(@RequestParam("data-entrada") String entrada,	@RequestParam("data-saida") String saida) {		

		Date inicio = FormatData.formataData(entrada + " 00:00");
		Date fim = FormatData.formataData(saida + " 23:59");;
		
		
		return viewRelatorioFinanceiroRepository.findByEntradaBetween(inicio , fim);

	

}
	
}
