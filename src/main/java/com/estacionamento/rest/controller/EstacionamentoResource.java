package com.estacionamento.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estacionamento.rest.model.Estacionamento;
import com.estacionamento.rest.service.EstacionamentoService;

@RestController
@RequestMapping("/estacionamentos")
public class EstacionamentoResource {

	@Autowired
	private EstacionamentoService estacionamentoService;
	
	@GetMapping("/{id}")
	public ResponseEntity<Estacionamento> findById(@PathVariable Long id) {
		return estacionamentoService.findById(id).map(estacionamento -> ResponseEntity.ok(estacionamento)).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@GetMapping
	public List<Estacionamento> findAll() {
		return estacionamentoService.findAll();
	}
	

	@PutMapping("/{id}")
	public ResponseEntity<Estacionamento> update(@RequestBody Estacionamento estacionamentoParam, @PathVariable Long id) {
		return estacionamentoService.findById(id).map(estacionamento -> {
			
			estacionamento.setNome(estacionamentoParam.getNome());
			estacionamento.setValorHora(estacionamentoParam.getValorHora());
			
			return ResponseEntity.ok(estacionamentoService.save(estacionamento));
		}).orElseGet(() -> ResponseEntity.notFound().build());
	}

}
