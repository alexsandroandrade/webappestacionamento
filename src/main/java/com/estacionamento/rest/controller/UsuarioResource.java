package com.estacionamento.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estacionamento.rest.model.Usuario;
import com.estacionamento.rest.service.UsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioResource {

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("/{id}")
	public ResponseEntity<Usuario> findById(@PathVariable Long id) {
		
		return usuarioService.findById(id).map(usuario -> ResponseEntity.ok(usuario)).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@GetMapping("/")
	public List<Usuario> findAll() {
		return usuarioService.findAll();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Usuario> update(@RequestBody Usuario usuarioParam, @PathVariable Long id){
		return usuarioService.findById(id).map(usuario -> {
			usuario.setLogin(usuarioParam.getLogin());
			usuario.setNome(usuarioParam.getNome());
			usuario.setSenha(usuarioParam.getSenha());
			return ResponseEntity.ok(usuarioService.save(usuario));
		}).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@PostMapping
	public Usuario cadastrar(@Valid @RequestBody Usuario usuario) {
		
		for( int pos = 0 ; pos < usuario.getTelefonesList().size() ; pos++ ) {
			usuario.getTelefonesList().get(pos).setUsuario(usuario);
		}
		
		return usuarioService.save(usuario);
	}
	
	@DeleteMapping("/{id}") 
	public ResponseEntity<Object> delete(@PathVariable Long id){
		return usuarioService.findById(id).map(usuario -> {
			usuarioService.delete(usuario);
			return ResponseEntity.noContent().build(); 
		}).orElseGet(() -> ResponseEntity.notFound().build());
	}

}
