package com.estacionamento.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estacionamento.rest.model.Ticket;
import com.estacionamento.rest.service.TicketService;

@RestController
@RequestMapping("/tickets")
public class TicketResource {
	@Autowired
	private TicketService ticketService;

	@PostMapping
	public ResponseEntity<Ticket> criarTicket(@RequestBody Ticket ticket) {

		return ticketService.criarTicket(ticket);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Ticket> encerrarTicket(@PathVariable Long id) {

		
		return ticketService.encerrarTicket(id);
	}

}
