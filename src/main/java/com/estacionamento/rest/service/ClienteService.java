package com.estacionamento.rest.service;

import java.util.List;
import java.util.Optional;

import com.estacionamento.rest.model.Cliente;

public interface ClienteService {

	Optional<Cliente> findById(Long id);
	
	List<Cliente> findAll();
	
	Cliente save(Cliente usuario);

	void delete(Cliente usuario);


}
