package com.estacionamento.rest.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estacionamento.rest.model.Estacionamento;
import com.estacionamento.rest.repository.EstacionamentoRepository;

@Service
public class EstacionamentoServiceImpl implements EstacionamentoService {
	
	@Autowired
	EstacionamentoRepository repository;

	@Override
	public Estacionamento save(Estacionamento estacionamento) {
		// TODO Auto-generated method stub
		return repository.save(estacionamento);
	}

	@Override
	public Optional<Estacionamento> findById(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
	}

	@Override
	public List<Estacionamento> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

}
