package com.estacionamento.rest.service;

import java.util.List;
import java.util.Optional;

import com.estacionamento.rest.model.Estacionamento;

public interface EstacionamentoService {
	
	Optional<Estacionamento> findById(Long id);
	/**
	 * Cria um novo Estacionamento
	 * @author alexsandro andrade
	 * 
	 */
	Estacionamento save(Estacionamento estacionamento);
	/**
	 * Busca uma lista com todos os Estacionamentos no banco
	 * @author alexsandro andrade
	 * 
	 */
	List<Estacionamento> findAll();

}
