package com.estacionamento.rest.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estacionamento.rest.model.Cliente;
import com.estacionamento.rest.repository.ClienteRepository;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	ClienteRepository clienteRepository;
	
	
	/***
	 * 
	 * @return objeto usuario encontrado
	 */
	@Override
	public Optional<Cliente> findById(Long id) {
		return clienteRepository.findById(id);
	}

	@Override
	public List<Cliente> findAll() {
		return clienteRepository.findAll();
	}

	@Override
	public Cliente save(Cliente usuario) {
		return clienteRepository.save(usuario);
	}

	@Override
	public void delete(Cliente usuario) {
		clienteRepository.delete(usuario);
	}

}
