package com.estacionamento.rest.service;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.estacionamento.rest.model.Ticket;

public interface TicketService {

	Optional<Ticket> findById(Long id);

	List<Ticket> findAll();
	/**
	 * Cria um novo Ticket para o Cliente que esta locando uma vaga no estacionamento ocupando uma Vaga
	 * @author alexsandro andrade
	 * @param ticket
	 */
	ResponseEntity<Ticket> criarTicket(Ticket ticket);
	/**
	 * encerra um Ticket para o Cliente acoplando a data de encerramento e o valor a ser pago pelo cliente e desocupando uma vaga
	 * @author alexsandro andrade
	 * 
	 */
	ResponseEntity<Ticket> encerrarTicket(Long id);


	void delete(Ticket ticket);

}
