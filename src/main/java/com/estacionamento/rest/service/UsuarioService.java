package com.estacionamento.rest.service;

import java.util.List;
import java.util.Optional;

import com.estacionamento.rest.model.Usuario;

public interface UsuarioService {

	Optional<Usuario> findById(Long id);
	
	List<Usuario> findAll();
	
	Usuario save(Usuario usuario);

	void delete(Usuario usuario);


}
