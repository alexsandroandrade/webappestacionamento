package com.estacionamento.rest.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.estacionamento.rest.model.Ticket;
import com.estacionamento.rest.model.Vaga;
import com.estacionamento.rest.model.Veiculo;
import com.estacionamento.rest.repository.TicketRepository;
import com.estacionamento.rest.repository.VagaRepository;

@Service
public class TicketServiceImpl implements TicketService {

	@Autowired
	private TicketRepository ticketRepository;
	@Autowired
	private VagaRepository vagaRespository;

	@Override
	public Optional<Ticket> findById(Long id) {
		return ticketRepository.findById(id);
	}

	@Override
	public List<Ticket> findAll() {
		return ticketRepository.findAll();
	}

	@Override
	public ResponseEntity<Ticket> criarTicket(Ticket ticket) {

		Optional<Vaga> vaga = vagaRespository.findById(ticket.getVaga().getId());

		if (vaga.isPresent()) {

			if ("LIVRE".equals(vaga.get().getStatus())) {

				Vaga tempVaga = vaga.get();
				
				Veiculo veiculo = new Veiculo();
				veiculo.setPlaca(ticket.getVeiculo().getPlaca());
				veiculo.setCor(ticket.getVeiculo().getCor());
				veiculo.setMarca(ticket.getVeiculo().getMarca());

				tempVaga.setStatus("OCUPADO");

				ticket.setEntrada(new Date());
				ticket.setVaga(tempVaga);
				ticket.setVeiculo(veiculo);
				return ResponseEntity.ok(ticketRepository.save(ticket));

			} else {
				return ResponseEntity.notFound().build();
			}

		} else {

			return ResponseEntity.notFound().build();
		}

	}

	@Override
	public void delete(Ticket ticket) {
		ticketRepository.delete(ticket);
	}

	@Override
	public ResponseEntity<Ticket> encerrarTicket(Long id) {

		Optional<Ticket> ticket = ticketRepository.findById(id);

		if (ticket.isPresent()) {

			Ticket tempTicket = ticket.get();

			tempTicket.getVaga().setStatus("LIVRE");
			tempTicket.setSaida(new Date());
			tempTicket.setValorTotalHora(calculaValorHora(tempTicket.getEntrada(), tempTicket.getSaida(),
					tempTicket.getVaga().getEstacionamento().getValorHora()));
			

			return ResponseEntity.ok(ticketRepository.save(tempTicket));

		} else {
			return ResponseEntity.notFound().build();

		}

	}

	private Double calculaValorHora(Date entrada, Date saida, Double valorHora) {

		Long tempoEmSegundos = (saida.getTime() - entrada.getTime()) / 1000;
		
		
		Double totalPagar = tempoEmSegundos * (valorHora / 3600);
		
		return totalPagar;

	}

}
