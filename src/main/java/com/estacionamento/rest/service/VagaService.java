package com.estacionamento.rest.service;

import java.util.List;
import java.util.Optional;

import com.estacionamento.rest.model.Vaga;

public interface VagaService {
	
	Optional<Vaga> findById(Long id);

	List<Vaga> findAll();

	Vaga save(Vaga vaga);

	void delete(Vaga vaga);
}
