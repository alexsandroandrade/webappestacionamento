package com.estacionamento.rest.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estacionamento.rest.model.Vaga;
import com.estacionamento.rest.repository.VagaRepository;

@Service
public class VagaServiceImpl implements VagaService {
	
	@Autowired
	VagaRepository repository;

	@Override
	public Optional<Vaga> findById(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id);
	}

	@Override
	public List<Vaga> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Vaga save(Vaga vaga) {
		// TODO Auto-generated method stub
		vaga.setStatus("LIVRE");
		return repository.save(vaga);
	}

	@Override
	public void delete(Vaga vaga) {
		// TODO Auto-generated method stub
		repository.delete(vaga);
	}

}
