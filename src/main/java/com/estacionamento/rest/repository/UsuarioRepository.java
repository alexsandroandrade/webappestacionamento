package com.estacionamento.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.estacionamento.rest.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
