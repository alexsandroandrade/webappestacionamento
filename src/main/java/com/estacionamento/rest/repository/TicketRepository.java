package com.estacionamento.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.estacionamento.rest.model.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Long> {

	
	
	
}
