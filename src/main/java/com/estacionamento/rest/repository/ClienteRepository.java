package com.estacionamento.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.estacionamento.rest.model.Cliente;


public interface ClienteRepository extends JpaRepository<Cliente, Long>{

}
