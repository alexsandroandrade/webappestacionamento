package com.estacionamento.rest.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.estacionamento.rest.model.ViewRelatorioFinanceiro;


public interface ViewRelatorioFinanceiroRepository extends JpaRepository<ViewRelatorioFinanceiro, Long> {
	
	/**
	 * Exibe o valor total arrecadado dentro de um determinado periodo
	 * @param entrada
	 * @param saida
	 * @return
	 */
	List<ViewRelatorioFinanceiro> findByEntradaBetween(Date dataInicial, Date dataFinal);

}
