package com.estacionamento.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;



import com.estacionamento.rest.model.ViewRelatorioVagas;

public interface ViewRelatorioVagasRepository extends JpaRepository<ViewRelatorioVagas, Long>{
	
}
