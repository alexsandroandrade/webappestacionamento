package com.estacionamento.rest.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.estacionamento.rest.model.ViewRelatorioTempoMedio;

public interface ViewRelatorioTempoMedioRepository extends JpaRepository<ViewRelatorioTempoMedio, Long> {

	/**
	 * Exibe o tempo medio em que os veiculos permaneceram dentro do estacionamento
		 */
	List<ViewRelatorioTempoMedio> findByEntradaBetween(Date entrada, Date saida);


}
