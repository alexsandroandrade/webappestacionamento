package com.estacionamento.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.estacionamento.rest.model.Vaga;

public interface VagaRepository extends JpaRepository<Vaga, Long> {

}
