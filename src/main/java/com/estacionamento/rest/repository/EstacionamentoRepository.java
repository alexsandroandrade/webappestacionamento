package com.estacionamento.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.estacionamento.rest.model.Estacionamento;

public interface EstacionamentoRepository extends JpaRepository<Estacionamento, Long>{

}
