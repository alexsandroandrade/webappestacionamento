package com.estacionamento.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.estacionamento.rest.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
